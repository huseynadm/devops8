1.Create New Docker Network
- docker network create devops8
2.Firts container must write to /mnt/index.html time every 5s
- Write Script (Script Name : date.sh) :

#!/bin/bash
clear
while true; do sleep 5; date > /mnt/index.html; done

- Use This Script on Dockerfile :

FROM ubuntu:latest
RUN apt-get update -y && \
    apt-get install -y cron && cron
ADD date.sh /tmp/date.sh
RUN chmod +x /tmp/date.sh
ENTRYPOINT ["/tmp/date.sh"]

- Build Image from this Dockerfile :

docker build -t writer_ubuntu:v1.0 .

- Run Container from this images :

docker run -d -v /mnt/:/mnt/ --network devops8 --name writer writer_ubuntu:v1.0

3.Second container must be nginx and use index from previous container

- Create Nginx Container use /mnt Volume

docker run -d -p 80:80 -v /mnt/:/usr/share/nginx/html/ --network devops8 nginx:latest


4.Third container also is nginx and this web server must forward all web reguests to
another nginx container.

- Create Nginx Log Container using the container's log file on the host : 

docker run -d -v /var/lib/docker/containers/Container-ID/Container-ID-json.log/:/var/log/nginx/nginx.log --name nginxlogger nginx:latest


