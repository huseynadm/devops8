## 1.Run Nexus Container

docker run -d -p 8081:8081 -p 8085:8085 --name nexus -v nexus-data:/nexus-data sonatype/docker-nexus3

## 2.Run Gitlab Container

mkdir -p /root/gitlab

docker run  \
-p 443:443 -p 80:80 -p 2222:22 \
--name gitlab \
--restart unless-stopped \
-v /root/gitlab/config:/etc/gitlab \
-v /root/gitlab/logs:/var/log/gitlab \
-v /root/gitlab/data:/var/opt/gitlab \
gitlab/gitlab-ce:latest

## 3.Run Gitlab Runner Container


mkdir -p /root/gitlab-runner
docker run -d --name gitlab-runner --restart always \
    -v /root/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest

## 4.Register Gitlab-Runner on Gitlab




## 5. Build & Push Docker Image to Nexus Repo with GitLab CI/CD

#5.1 Create New Project
#5.2 Create .gitlab-ci.yaml & Dockerfile in Project
#5.3 Add Gitlab Variable for .gitlab-ci.yaml
#5.4 Run Pipeline
